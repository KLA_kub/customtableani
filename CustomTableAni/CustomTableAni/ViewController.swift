//
//  ViewController.swift
//  CustomTableAni
//
//  Created by Blackwolf on 6/14/2559 BE.
//  Copyright © 2559 Blackwolf. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var buttonNext: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func buttonClick(sender: AnyObject) {
        getData{ (result,error )in
        let vc : TableViewController = self.storyboard!.instantiateViewControllerWithIdentifier("TableList") as! TableViewController

        vc.list += result

        
        vc.modalTransitionStyle = UIModalTransitionStyle.CoverVertical
        self.presentViewController(vc, animated: true, completion: nil)
        }
    }
}

