//
//  Model.swift
//  CustomTableAni
//
//  Created by Blackwolf on 6/14/2559 BE.
//  Copyright © 2559 Blackwolf. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire


public func getData(completionHandler:(result:Array<String>,error:String) ->()){
    
    Alamofire.request(.GET,"http://203.151.232.161/test/wv.php",
        parameters: [:],
        encoding: ParameterEncoding.URL, headers: nil).responseJSON{ response in
            
            var array:Array<String> = Array<String>()
            if response.result.value != nil{
                let value = response.result.value
                let json = JSON(value!)
                let item = json["items"].array
                
                for i in 0..<item!.count{
                array += [item![i]["imageUrl"].stringValue]
                }
                completionHandler(result:array , error: "Gundam")
            }
    }
}